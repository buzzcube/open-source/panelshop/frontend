import Vue from "vue";
import auth0 from "auth0-js";
import router from "../../router";

const state = {
  userIsAuthorized: true,
  auth0: new auth0.WebAuth({
    domain: process.env.VUE_APP_AUTH0_CONFIG_DOMAIN,
    clientID: process.env.VUE_APP_AUTH0_CONFIG_CLIENTID,
    redirectUri: process.env.VUE_APP_DOMAINURL + "/auth0callback",
    responseType: process.env.VUE_APP_AUTH0_CONFIG_RESPONSETYPE,
    scope: process.env.VUE_APP_AUTH0_CONFIG_SCOPE,
    audience: process.env.VUE_APP_AUTH0_CONFIG_AUDIENCE,
    leeway: 10
  }),
  user: undefined,
  accessToken: undefined
};

const getters = {
  userAvatarUrl(state) {
    if (state.user && state.user.picture) {
      return state.user.picture;
    }
    return "/static/avatars/avatar-placeholder.png";
  },
  userIsAuthorized(state) {
    return state.userIsAuthorized;
  },
  user(state) {
    return state.user;
  },
  accessToken(state) {
    return state.accessToken;
  }
};

const actions = {
  auth0Login(context) {
    context.state.auth0.authorize();
  },
  auth0HandleAuthentication(context) {
    context.state.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken && authResult.idTokenPayload) {
        let user = JSON.stringify(authResult.idTokenPayload);
        let expiresAt = JSON.stringify(authResult.expiresIn * 1000 + new Date().getTime());
        localStorage.setItem("access_token", authResult.accessToken);
        localStorage.setItem("id_token", authResult.idToken);
        localStorage.setItem("expires_at", expiresAt);
        localStorage.setItem("user", user);
        context.state.accessToken = authResult.accessToken;
        router.replace("/");
      } else if (err) {
        // alert("Login failed.");
        // router.replace("/");
        console.log(err);
      }
    });
  },
  // action to handle re-authentications when the token expires
  auth0HandleAuthenticationSilently(context) {
    context.state.auth0.parseHash((err, authResult) => {
      if (authResult && authResult.accessToken && authResult.idToken && authResult.idTokenPayload) {
        let user = JSON.stringify(authResult.idTokenPayload);
        let expiresAt = JSON.stringify(authResult.expiresIn * 1000 + new Date().getTime());
        localStorage.setItem("access_token", authResult.accessToken);
        localStorage.setItem("id_token", authResult.idToken);
        localStorage.setItem("expires_at", expiresAt);
        localStorage.setItem("user", user);
        // this.dispatch("getRMSCredentials");
      } else if (err) {
        //alert("Authentication failed.");
        console.log(err);
      }
    });
  },
  /* eslint-disable no-unused-vars */
  auth0Logout(context) {
    localStorage.removeItem("access_token");
    localStorage.removeItem("id_token");
    localStorage.removeItem("expires_at");
    localStorage.removeItem("user");

    window.location.href =
      process.env.VUE_APP_AUTH0_CONFIG_DOMAINURL +
      "/v2/logout?returnTo=" +
      process.env.VUE_APP_DOMAINURL +
      "&client_id=" +
      process.env.VUE_APP_AUTH0_CONFIG_CLIENTID;
  }
};

const mutations = {
  setUserIsAuthenticated(state, replacement) {
    state.userIsAuthorized = replacement;
  },
  setUser(state, replacement) {
    state.user = replacement;
  },
  setAccessToken(state, replacement) {
    state.accessToken = replacement;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
