import Vue from "vue";
import Vuex from "vuex";

// modules
import auth from "./auth";
import config from "./config";

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    auth,
    config
  }
});
