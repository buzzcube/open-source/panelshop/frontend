import Vue from "vue";

const state = {
  packageVersion: JSON.parse(unescape(process.env.PACKAGE_JSON || "%7Bversion%3A0%7D")).version
};

const getters = {
  appVersion: state => {
    return state.packageVersion;
  }
};

const actions = {};

const mutations = {};

export default {
  state,
  getters,
  actions,
  mutations
};
