import axios from "axios";
import { store } from "../store";

function getPublicStartupBattles() {
  const url = `${process.env.VUE_APP_API_URL}/api/battles/public`;
  return axios.get(url).then(response => response.data);
}

function getPrivateStartupBattles() {
  const url = `${process.env.VUE_APP_API_URL}/api/battles/private`;
  const accessToken = store.state.auth.accessToken;
  const headers = {
    headers: { Authorization: `Bearer ${accessToken}` }
  };
  return axios.get(url, headers).then(response => response.data);
}

export { getPublicStartupBattles, getPrivateStartupBattles };
