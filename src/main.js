import Vue from "vue";
import bugsnagClient from "./plugins/bugsnag";
import bugsnagVue from "@bugsnag/plugin-vue";
import "./plugins/vuetify";
import App from "./App.vue";
import router from "./router";
import { store } from "./store";

Vue.config.productionTip = false;

bugsnagClient.use(bugsnagVue, Vue);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
