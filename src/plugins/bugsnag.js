import bugsnag from "@bugsnag/js";
var bugsnagClient = bugsnag({
  apiKey: process.env.VUE_APP_BUGSNAG_KEY,
  otherOptions: {
    appVersion: JSON.parse(unescape(process.env.PACKAGE_JSON || "%7Bversion%3A0%7D")).version,
    releaseStage: process.env.NODE_ENV
  },
  beforeSend: function(report) {
    report.user = JSON.parse(localStorage.getItem("user"));
  }
});
export default bugsnagClient;
